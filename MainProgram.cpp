#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <ostream>
#include "Dino_Game.cpp"
#include <thread>

using namespace cv;
using namespace std;

int cnt = 0;

char inputChar;

int min_area = 200; //min area of motion detectable

//get camera operational and make sure working correctly
VideoCapture camera(1);

Mat firstFrame, gray, imageDifference, thresh, frameFlip, roi1Flip;
vector<vector<Point> > contours;
vector<Vec4i> hierarchy;


    void runControl() {
        while (true) {
            Mat frame;
            camera.read(frame);

            Mat roi1 = frame(Range(150, 200), Range(100, 150));
            rectangle(frame, Point(100, 150), Point(150, 200), Scalar(0, 255, 0), 2);

            //pre processing
            //resize(frame, frame, Size (1200,900));
            flip(frame, frameFlip, 1);
            flip(roi1, roi1Flip, 1);
            cvtColor(roi1Flip, gray, COLOR_BGR2GRAY);
            GaussianBlur(gray, gray, Size(21, 21), 0, 0);


            //initrialize first frame if necessary
            if (firstFrame.empty()) {
                if (cnt < 20) {
                    ++cnt;
                }
                else {
                    cout << "hit" << endl;
                    gray.copyTo(firstFrame);
                }
                continue;
            }

            //get difference
            absdiff(firstFrame, gray, imageDifference);
            threshold(imageDifference, thresh, 25, 255, THRESH_BINARY);
            //fill in holes
            dilate(thresh, thresh, Mat(), Point(-1, -1), 2, 1, 1);
            findContours(thresh, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

            //loop over contours
            for (int i = 0; i < contours.size(); i++) {
                //get the boundboxes and save the ROI as an Image
                if (contourArea(contours[i]) < min_area) {
                    continue;
                }
                Rect boundRect = boundingRect(Mat(contours[i]));
                rectangle(roi1Flip, boundRect.tl(), boundRect.br(), Scalar(0, 255, 0), 1, 8, 0);

                //cout << "Jump!" << endl;

                inputChar = 'j';

            }

            if (inputChar == 'j'){
                inputChar = ' ';
            }

            //draw everything
            imshow("mainWindow", frameFlip);
            imshow("ROI", roi1Flip);
            if (waitKey(30) >= 0)
                break;

        }
    }

    int getCh() {
        return inputChar;
    }

    int runGame() {

        system("mode con: lines=29 cols=82");
        char ch;
        int i;
        getup();


        delay(3000);

        while (true)
        {
            while (!_kbhit())
            {
                ds();
                obj();
            }

            ch = getCh();

            if (ch == 'j')
            {
                for (i = 0; i < 10; i++)
                {
                    ds(1);
                    obj();
                }
                for (i = 0; i < 10; i++)
                {
                    ds(2);
                    obj();
                }
            }
            else if (ch == 'x')
                return(0);
        }
    }

int main() {

    namedWindow("mainWindow");
    namedWindow("ROI");



    thread first(runControl);
    thread second(runGame);


    

    destroyAllWindows();
    return(0);

}